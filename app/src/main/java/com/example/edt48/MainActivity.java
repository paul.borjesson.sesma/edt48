package com.example.edt48;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Picture> pictures = new ArrayList<>();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rv);
        initArrayPicture();
        MyAdapter myAdapter = new MyAdapter(pictures, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void initArrayPicture() {
        for (int i = 1; i <= 38; i++) {
            pictures.add(new Picture("Pic" + i,"https://joanseculi.com/images/img" + format(i) + ".jpg", "Desc" + i));
        }
    }

    private String format(int val) {
        if (val < 10) {
            return "0" + String.valueOf(val);
        }
        return String.valueOf(val);
    }

}