package com.example.edt48;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class Inside extends AppCompatActivity {

    private TextView textViewText;
    private TextView textViewDesc;
    private ImageView imageViewUrl;

    String text, desc, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inside);

        textViewText = findViewById(R.id.textViewText);
        textViewDesc = findViewById(R.id.textViewDesc);
        imageViewUrl = findViewById(R.id.imageViewUrl);

        getData();
        setData();

    }

    private void getData() {
        if (getIntent().hasExtra("text") &&
                getIntent().hasExtra("desc")) {
            text = getIntent().getStringExtra("text");
            desc = getIntent().getStringExtra("desc");
            url = getIntent().getStringExtra("url");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        textViewText.setText(text);
        textViewDesc.setText(desc);
        Picasso.get().load(url)
                .fit()
                .centerCrop()
                .into(imageViewUrl);
    }

}