package com.example.edt48;

public class Picture {

    private String text;
    private String url;
    private String desc;

    public Picture(String text, String url, String desc) {
        this.text = text;
        this.url = url;
        this.desc = desc;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    public String getDesc() {
        return desc;
    }
}
